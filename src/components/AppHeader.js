import React, { Component } from 'react'
import { version } from '../../package.json'
import { Row, Col } from 'antd'

export default class AppHeader extends Component {

    render() {
        return (
            <Row>
                <Col className="App-origin" xs={14} sm={14} md={14} lg={4}>Made by<br /><a className="external-link" rel="noopener noreferrer" href="http://www.IT-Happens.de" target="_blank">IT-Happens.de</a></Col>
                <Col lg={8}>
                    <Row className="App-header application-title" >The.Run</Row>
                    <Row>
                        <Col xs={12} sm={6} md={4} lg={2}><a className="external-link" rel="noopener noreferrer" href="https://bitbucket.org/uncleryppo/therun-webclient/wiki/Home" target="_blank">Help</a></Col>
                        <Col xs={12} sm={6} md={4} lg={4} />
                        <Col xs={12} sm={6} md={4} lg={2}><p className="external-link">{version}</p></ Col>
                    </Row>
                </Col>
            </Row>
        )
    }

}