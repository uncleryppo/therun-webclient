import { Layout, Row } from 'antd'
import React, { Component } from 'react'
import '../style/App.css'
import AppHeader from './AppHeader'
import ContentViewv from './ContentView'
import ParticipationsStore from '../stores/participations-store'
import {observer} from 'mobx-react'

const { Header, Content } = Layout
const participationsStore = new ParticipationsStore()

export default observer(class App extends Component {

  componentDidMount() {
    participationsStore.fetchParticipations()
  }

  render() {
    return (
      <Layout>
        <Header>
          <Row className="App-header">
            <AppHeader />
          </Row>
        </Header>
        <Content>
          <ContentViewv participationsStore={participationsStore} />
        </Content>
      </Layout>
    )
  }
})
